var train = document.getElementById("train");
train.addEventListener('click', move);

var stopBtn = document.getElementById("stop");
stopBtn.addEventListener('click', stop);

var width = 0;
var setTime;
var click = 0;

function move() {
    setTime = setInterval(setMove, 300);
}

function setMove() {
    if (width == 88) {
        clearInterval(setTime);
        alert("loose");
    } else {
        width++;
        train.style.marginLeft = width + "%";
    }
}

function stop() {
    clearInterval(setTime);
    alert("win");
}